import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AppLogin } from 'src/app/interfaces/interfaces';
import { environment } from 'src/environments/environment';
import { Router } from '@angular/router';
import { JwtHelperService } from '@auth0/angular-jwt';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  apiUrl = environment.apiUrl;

  constructor(private router: Router, private http: HttpClient, private jwtHelperService: JwtHelperService) {}

  public getToken(): string {
    return localStorage.getItem('token');
  }

  login(body: AppLogin) {
    return this.http.post<any>(`${this.apiUrl}/login`, body);
  }

  logout() {
    localStorage.removeItem('token');
  }

  public isLoggedIn(): boolean {
    // get the token
    const token = localStorage.getItem('token');
    // return a boolean reflecting
    // whether or not the token is expired
    if (token == null) {
      return false;
    } else {
      return !this.jwtHelperService.isTokenExpired(token);
    }
  }

  getStatus() {
    return this.http.get<any>(`${this.apiUrl}/status/authenticated`);
  }
}
