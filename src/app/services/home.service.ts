import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AppPost } from 'src/app/interfaces/interfaces';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root',
})
export class HomeService {
  apiUrl = environment.apiUrl;
  constructor(private http: HttpClient) {}

  getPosts() {
    return this.http.get<AppPost[]>('https://jsonplaceholder.typicode.com/posts');
  }
}
