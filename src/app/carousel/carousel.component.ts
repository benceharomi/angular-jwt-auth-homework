import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-carousel',
  templateUrl: './carousel.component.html',
  styleUrls: ['./carousel.component.scss'],
})
export class CarouselComponent {
  images = [944, 1011, 984].map(n => `https://placekitten.com/${n}/600`);
  constructor() {}
}
