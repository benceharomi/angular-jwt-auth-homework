import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AuthService } from '../auth/auth.service';
import { take } from 'rxjs/operators';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  error: string;
  constructor(private fb: FormBuilder, private authenticationService: AuthService, private router: Router) {}

  ngOnInit() {
    this.loginForm = this.fb.group({
      mail: ['', [Validators.email, Validators.required]],
      password: ['', [Validators.minLength(8), Validators.required]],
    });
  }

  onSubmit() {
    this.authenticationService
      .login({
        ...this.loginForm.value,
      })
      .pipe(take(1))
      .subscribe(
        res => {
          localStorage.setItem('token', res.token);
          this.router.navigate(['profile']);
        },
        err => {
          console.log(err);
          this.error = err.message;
        },
      );
  }
}
