export interface AppPost {
  body: string;
  id: number;
  title: string;
  userId: number;
}

export interface AppLogin {
  mail: string;
  password: string;
}
