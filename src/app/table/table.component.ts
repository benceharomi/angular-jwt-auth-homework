import { Component, OnInit, PipeTransform } from '@angular/core';
import { HomeService } from '../services/home.service';
import { Observable, combineLatest } from 'rxjs';
import { AppPost } from '../interfaces/interfaces';
import { FormControl } from '@angular/forms';
import { startWith, map } from 'rxjs/operators';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss'],
})
export class TableComponent implements OnInit {
  items$: Observable<AppPost[]>;
  filteredItems$: Observable<AppPost[]>;
  filter: FormControl;
  filter$: Observable<string>;
  page = 1;
  pageSize = 10;

  constructor(private homeService: HomeService) {}

  ngOnInit() {
    this.items$ = this.homeService.getPosts();
    this.filter = new FormControl('');
    this.filter$ = this.filter.valueChanges.pipe(startWith(''));
    this.filteredItems$ = combineLatest(this.items$, this.filter$).pipe(
      map(([items, filterString]) =>
        items.filter(item => item.title.toLowerCase().indexOf(filterString.toLowerCase()) !== -1),
      ),
    );
  }
}
