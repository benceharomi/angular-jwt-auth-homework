import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth/auth.service';
import { take } from 'rxjs/operators';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss'],
})
export class ProfileComponent implements OnInit {
  status = this.authService
    .getStatus()
    .pipe(take(1))
    .subscribe(
      res => (this.status = res.status),
      err => (this.status = err.statusText),
    );

  constructor(private authService: AuthService) {}

  ngOnInit() {}
}
