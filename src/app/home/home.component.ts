import { Component } from '@angular/core';
import { HomeService } from '../services/home.service';
import { AuthService } from '../auth/auth.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ModalComponent } from '../modal/modal.component';
import { Observable } from 'rxjs';
import { AppPost } from '../interfaces/interfaces';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class HomeComponent {
  postArray$ = this.authService.getStatus();
  items$: Observable<AppPost[]>;
  filteredItems$: Observable<AppPost[]>;
  filter: FormControl;
  filter$: Observable<string>;
  page = 1;
  pageSize = 10;

  constructor(private homeService: HomeService, private authService: AuthService, private modalService: NgbModal) {}

  open() {
    this.modalService.open(ModalComponent, { size: 'lg' });
  }
}
